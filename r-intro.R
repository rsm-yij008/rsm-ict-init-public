## start by making sure your environment is empty
## use Session > Restart R or the keyboard shortcuts
## macOS: Shift + Cmd + 0
## windows: Shift + Ctrl + F10
## To clear the terminal use:
## macOS and Windows: Ctrl + L
## To execute a line of code use:
## macOS: Cmd + Enter
## windows: Ctrl + Enter

## to load data R must know where the data is located (i.e., the path)

## hint: find the absolute or full 'path' using ...
full_path <- file.choose()
full_path

## but we probably want the relative path
"data/houseprices.rds"

## generate R-code to load a file
library(radiant.data)
read_files(type = "R", clipboard = FALSE)

## recreate the code below using "read_files"
# houseprices2 <- readr::read_rds("data/houseprices2.rds")

## best approach is to use Rstudio projects and 'relative' paths
getwd()

?setwd

## explore what "fs" does
?fs
fs::path_real("~")
fs::path_real(".")
fs::path_real("..")
fs::path_real("data")

fs::path_real("../test1")

fs::dir_ls("data")
fs::dir_ls("../test1")

## generate a relative path from an absolute path
fs::path_rel(full_path, start = ".")
fs::path_rel("~/Dropbox/ICT2019", start = ".")

## what if the data is on Dropbox or Google drive? How to share data?
## what is the results produced by the commands below?
radiant.data::find_dropbox()
# radiant.data::find_gdrive()

fp <- fs::path(radiant.data::find_dropbox(), "ICT2019")
fs::dir_exists(fp)

## is fp a full or a relative path? 
## is fp appropriate for reproducible analysis?
fp

## load the houseprices.rds file from the data directory on Dropbox
?readr::read_rds
?readr::write_rds

houseprices <- readr::read_rds("data/houseprices.rds")

readr::read_rds(fs::path(fp, "data/houseprices.rds"))

## view data
View(houseprices)

## save the data to houseprices2.rds in the Rstudio project data directory
readr::write_rds(houseprices, path = "data/houseprices2.rds")

## will this work? why? why not?
readr::write_rds(houseprices, path = fs::path(fp, "data/houseprices2.rds"))

## remove data from memory
rm(houseprices)
View(houseprices)

## ... or use
houseprices <- NULL
View(houseprices)

## load houseprices.rda from the Rstudio project data directory
## what happened? Hint: Use ls() and View()
load("data/houseprices.rda")

## loading "raw" data
?readr::read_csv
?readr::write_csv

## load the sales.csv data in the Dropbox data directory
readr::read_csv(fs::path(fp, "data/sales.csv"))

## what happened?
## assign the data to a variable
sales <- readr::read_csv(fs::path(fp, "data/sales.csv"))

## Note: text printed in red does not mean there is an error!

## (dis)advantages of rds format?
## smaller (if compression is used)
## maintains variable specs (e.g., factor, date)
## vs rda, assign to a variable so you know what happended
readr::write_rds(sales, "data/sales.rds", compress = "gz")

## read excel file portfolio.xlsx from the Dropbox folder
## using "Import Dataset" in the Environment tab
help(package = "readxl")
portfolio <- readxl::read_excel(fs::path(fp, "data/portfolio.xlsx"), sheet = "Data")
portfolio

## collections of numbers (or characters) can be stored in a data structure,
## e.g., a vector
## c combines values into a vector
c(3, 4, 5)
c("a", "b", 5)

## your turn ... what happened to "5"?

## provide names for each element in a vector, i.e., a "named vector"
named_vector <- c(price_a = 1, price_b = 2, price_c = 3)
named_vector

named_vector["price_b"]
named_vector[2]

## your turn ...
## create a vector of numbers from 1 to 5, assign the vector to a variable x,
## and print x
?seq

## your turn ...
## create a vector of length 10 where each value is equal to 1
?rep

## other data structures
## vector, list, tibble (aka data frame), matrix, array
## use class() and typeof() to determine type

## recycling
1 / c(1, 2, 3)
c(1, 2, 3) / c(1, 2, 3)
c(1, 2, 3) / 3
c(1, 2) / c(1, 2, 3, 4)
c(1, 2) / c(1, 2, 3) ## This still does something!

## indexing (i.e., accessing specific elements in an object)
x <- c(a = 10, b = 11, c = 12)

## for single element
x[3]
x["b"]

## what happens when you run the line below?
x$b

## select multiple elements
x[c(1, 3)]
x[2:3]
x[c("a", "c")]

## your turn ..
## show the first and last element of x
## can you think of different ways to do this?

## selecting with a logical vector
x <- 101:110
x[x > 105]

## your turn ...
## get the index of values in x > 105
## and show those values

## your turn ...
## apply sum and mean to x > 5
## what happens?
lval <- x > 105
mean(lval)
sum(lval)

## creating a tibble (aka a data.frame)
library(tibble)
browseURL("http://r4ds.had.co.nz/tibbles.html")

?tibble
tbl <- tibble(a = 1:3, b = c("a", "b", "c"))
class(tbl)
typeof(tbl)
tbl

## your turn ...
## find the class for each column in tbl

## display the "structure" of the tibble
?str
str(tbl)
tbl

## indexing a tibble (aka data.frame)
tbl[, 1]
tbl[, 1:2]
tbl[1]

## compare the return values from the above 3 lines
## with the below 3 lines
tbl[[1]]
tbl[["a"]]
tbl$a

## your turn ...
## what is the class of tbl[1, ]? What about tbl[[1]]?
## suppose the name of the column to extract is assigned to a variable "col"?
## what is the class of tbl[col, ]? What about tbl[[col]]?
col <- "a"

## select the 2nd row from tbl

## your turn ...
## can you make the 2nd row from tbl into a vector?
## what is the challenge here?

## lists
x <- list(10, 11, c(12, 13, 14))
x
x[1]
x[3]
x[[3]]

## lists can also be named
x <- list(a = 1:10, b = letters)
x[["b"]]
x$b

## your turn ...
## could x, as specified above, be converted to a tibble?
## why or why not?
?as_tibble

## the matrix type
m <- matrix(1:10, nrow = 5, ncol = 2)
m[2, ]
m[, 2]
class(m[, 2])
class(m[, 1:2])
m[, 2, drop = FALSE]

## making an array
array(1:12, c(2, 2, 3))

## your turn ... create a 2 x 2 x 2 array of missing values (NA)
?NA

## why does this work?

## changing types
x <- c(1, 2, 3, 4, 5)
class(x)
is.integer(x)
x <- as.integer(x)
class(x)
x <- c(1L, 2L, 3L, 4L, 5L)
class(x)

## your turn ...
## convert y to a numeric vector
y <- c("1.2", "2.9", "3.0")

## what happens if you try to convert y to integer?
## note: as.double is identical to as.numeric

## drop the first element of x
x[-1]

## drop the first two elements
x[-1:-2]
x[-c(1:2)]
x[-(1:2)]

# drop just the last element
x[-length(x)]

## drop the first row or column
tbl[-1, ]
tbl[, -1]

## your turn ...
## use head to drop the last row of tbl
## use tail to drop the first row of tbl

## your turn ...
## use head and tail to get c:g from the 'letters' vector

## your turn ...
## make a copy of mtcars (call it dat) and
## convert it to a tibble

## your turn ...
## turn the 2nd column of dat into an integer

## any alternatives?

## your turn ...
## turn "vs" in dat into a factor

## your turn ...
## does dat have any missing values?
?is.na
?any

## your turn ...
## set the value in the 3rd row in the 2nd column of dat to a missing value (NA)
## and write a check to determine if dat has *any* missing values
dat[3, 2] <- NA

## use sum to determine how many NAs are there in dat

## create a factor of letters from a to g with levels in the order g to a
?factor
?rev

# if else and ifelse
x <- 2
if (x == 1) {
  "x is 1"
} else if (x == 2) {
  "x is 2"
} else {
  "x is not 1 or 2"
}

x <- 1
ifelse(x == 1, "x is 1", "x is not 1")

## your turn ...
## why does the ifelse statement below print 1?
x <- 1:5
ifelse(length(x) > 1, x, "x is not longer than 1")
?ifelse

## use an if else setup instead. Does that work?

## comparisons
?`==`

a <- b <- 1
a == b
a != b
a > b
a < b
a <= b

a > b || b == a

## | and & are vectorized versions of || and &&
a <- 1:10
b <- 5
a < b | a == b

## your turn ...
## write a more compact version of a < b | a == b

## what does %in% do?
## explain in words what is different between the lines below
?`%in%`
"c" %in% letters
letters %in% c("c", "z")

## iteration
for (i in 1:10) {
  print(i)
}

for (i in 1:10) {
  i
}

for (i in letters) {
  print(paste0("The letter ", i))
}

for (i in 1:length(letters)) {
  print(letters[i])
}

for (i in seq_along(letters)) {
  print(letters[i])
}

letters0 <- c()

## what is happening below?
for (i in 1:length(letters0)) {
  print(letters0[i])
}

## why doesn't the same thing happen here?
for (i in seq_along(letters0)) {
  print(letters0[i])
}

## while loop
i <- 1
while (i <= 10) {
  print(i)
  i <- i + 1
}

## your turn ...
## use a for loop to print the diagonal elements of the following matrix
m <- matrix(1:16, nrow = 4, ncol = 4)
m

## your turn ... use the diag function to do the same
?diag
diag(m)

## the apply family of functions
?sapply
for (i in letters) print(i)
ret <- sapply(letters, print)

## using purrr
library(purrr)
resp <- map_chr(letters, print)

## your turn ...
## calculate the mean for each column in mtcars using purrr::map_dbl
## what is different it you use purrr::map_df

## your turn ...
## use purrr::map_dbl to calculate the coefficient of variation
## using your own function

## your turn ...
## use map_chr to show "a is a letter" for all elements in 'letters'
## output should be the same as the paste0 command below
?paste0
paste0(letters, " is a letter")
?purrr::map_chr

## your turn ...
## generate a histogram (use hist) for each column using purrr::map

## your turn ... generate a scatter plot (use plot) for each column vs mtcars$mpg using purrr::map
resp <- map(mtcars, plot, mtcars$mpg)
?map

## pipes have been around for a long time - particularly in the unix ecosystem
## a pipe passes the result from one tool to other tools as input
## magrittr introduced the %>% pipe operator to R
## passes result from expression on the left as the *first* argument to the
## expression on the right
## lets see some examples of why this is useful ...

## read the code "inside-out"
spent <- c("RMB100.3", "RMB200.5", "RMB300.8")
total <- round(sum(as.numeric(sub("RMB", "", spent))), 0)
total

## or ...
spent <- c("RMB100.3", "RMB200.5", "RMB300.8")
spent <- sub("RMB", "", spent)
spent <- as.numeric(spent)
total <- sum(spent)
total <- round(total, 0)
total

## the example above is still hard to read and we have to type
## `spent` 6 times ... uggghhhh
## "pipes" produce an easier to read structure
library(magrittr)
total <- c("RMB100.3", "RMB200.5", "RMB300.8") %>%
  sub("RMB", "", .) %>%
  as.numeric() %>%
  sum() %>%
  round(0)
total

## can also refer to the previous result using `.`
1:5 %>% length(.)
tibble(a = 1:3, b = 3:1) %>% .[[1]]
tibble(a = 1:3, b = 3:1) %>% .[[ncol(.)]]
tibble(a = 1:3, b = 3:1) %>% dim(.)

## for more on the %>% operator
?magrittr::`%>%`

## your turn ...
## what do length, ncol, and dim do?
## explain in words
?length
?ncol
?dim

## what about other arguments?
## sometimes we want to send our results to a function argument *other* than first one
## or we want to use the previous result for multiple arguments.
## in these cases we refer to the previous result using `.`.
## if-statements in a chain
tibble(a = 1:3, b = 3:1) %>% 
  {if (dim(.)[2] > 0) .[[2]] else .[[1]]}

## your turn ...
## make a 'piped' version of the code below
x <- 1:5
c(sd(x), mean(x), median(x))

## functions
foo <- function(x, na.rm = FALSE) {
  result <- x - mean(x, na.rm = na.rm)
  return(result)
}

foo(1:5)
foo(c(1, 3, NA))
foo(c(1, 3, NA), na.rm = TRUE)

## using a function with map_dbl
cvar <- function(x, na.rm = FALSE) {
  sd(x, na.rm = na.rm) / mean(x, na.rm = na.rm)
}

map_dbl(mtcars, cvar)
dat <- as_tibble(mtcars)
dat[2, ] <- NA
map_dbl(dat, cvar)
map_dbl(dat, cvar, na.rm = TRUE)

## your turn ...
## write a function that returns am vs pm depending on the time of day
## set the default value for the time of day to lubridate::now()
library(lubridate)

now()
ctime <- hm("10:00")
ctime <- hm("13:30")
hour(ctime)

am_pm <- function() {
  ## put your code below
}
am_pm(hm("10:00")) ## should return AM
am_pm(hm("13:30")) ## should return PM
am_pm()

## simulate a profit function
price <- 8
sales <- 100 - 10 * price
cost <- 2
profit <- sales * (price - cost)

##  without using a loop
price <- seq(0, 12, .1)
profit <- (100 - 10 * price) * (price - 2)
tbl <- tibble(price = price, profit = profit)

## find the maximum
tbl[which.max(tbl$profit), ]
plot(tbl, type = "l")

## your turn ...
## use a function loop and a function
## to calculate the same result
## the function should calculate profits for a single
## value of price
tbl <- tibble(p = seq(0, 12, .1), profit = NA)
price <- seq(0, 12, .1)


tbl[which.max(tbl$profit), ]
plot(tbl, type = "l")

## printing function code
?radiant.data::find_dropbox
radiant.data::find_dropbox

## 'methods'
summary(c(1, 2, 3))
summary(iris)
summary(tbl)
class(tbl)
summary.data.frame
summary.lm

methods(summary)
methods(print)
?methods

mat <- matrix(1:100, nrow = 10)
summary.matrix(mat)
summary.matrix(c(1, 2, 3))

## getting help for specific methods
?radiant.model::summary.regress
?radiant.basics::summary.correlation


